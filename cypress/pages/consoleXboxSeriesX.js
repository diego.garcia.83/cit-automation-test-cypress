import { elements } from "./elements"

class consoleXboxSeriesX {

    validateTitleProduct() {

        cy.get(elements.lblTitleXbox)
          .should('to.be.visible')
          .should('contains.text', 'Console Xbox Series X')

    }

}

export default new consoleXboxSeriesX