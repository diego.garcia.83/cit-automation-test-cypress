exports.elements = {

    // COMPONENTS

    txtSelecAddress: '#glow-ingress-line2',

    // SEARCHBAR

    searchBar: '#twotabsearchtextbox',
    btnSubmit: '#nav-search-submit-button',

    // CONSOLE XBOX SERIES X

    lblTitleXbox: '#productTitle'
}