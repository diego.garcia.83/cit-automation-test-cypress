import topMenu from "../components/topMenu"
import searchBar from "../components/searchBar"
import findResults from "../pages/findResults"
import consoleXboxSeriesX from "../pages/consoleXboxSeriesX"

describe('Pesquisar Produto', () => {

    beforeEach(() => {
        cy.visit('/')
    })

    it('Validar Página Inicial', () => {

        topMenu.validHomePage()

    })

    it('Pesquisar Produto', () => {

        topMenu.validHomePage()
        searchBar.findProduct()
        searchBar.subimitButton()
        findResults.validateResults()

    })

    it('Acessar Produto Pesquisado', () => {

        topMenu.validHomePage()
        searchBar.findProduct()
        searchBar.subimitButton()
        findResults.validateResults()
        findResults.selectSearchedProduct()
        consoleXboxSeriesX.validateTitleProduct()

    })

})