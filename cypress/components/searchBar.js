import { elements } from "../pages/elements"

class searchBar {

    findProduct() {

        cy.get(elements.searchBar)
          .should('to.be.visible')
          .type('Xbox Series X')
          .should('have.value', 'Xbox Series X')

    }

    subimitButton() {

        cy.get(elements.btnSubmit)
          .should('to.be.visible')
          .click()

    }

}

export default new searchBar