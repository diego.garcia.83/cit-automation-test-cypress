const { defineConfig } = require("cypress")

module.exports = defineConfig({
  e2e: {
    setupNodeEvents(on, config) {
      // implement node event listeners here
    },
    baseUrl: 'https://amazon.com.br',
    video: false,
    chromeWebSecurity: false,
    viewportWidth: 1366,
    viewportHeight: 768
  },
})
