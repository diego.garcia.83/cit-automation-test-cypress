# cit-automation-test-cypress

Projeto responsável por automatizar a pesquisa de um produto no site da Amazon utilizando o framework Cypess


## Porque usar o Cypress

O Cypress é um framework de testes moderno e está se popularizando cada vez mais no mercado de automação. 

## Alguns motivos para escolha do Cypress:

Instalação Facilitada: Depois de clonar o projeto, basta somente um comando e tudo está pronto para executar os testes.

Curva de Aprendizado Reduzida: Por usar como base o JavaScript, a maioria dos automatizadores tem o conhecimento básico para criação dos testes.

Diferentes Tipos de Testes: Pode ser usado para testar WEB, API, testes de componentes e regressão visual.

Documentação: Documentação sempre atualizada e com informações de como usar as Boas Práticas de Programação.


## Pré-requisitos

Para executar esse projeto é necessário:

- git
- Node.js
- NPM


## Instalação do prejeto

`npm install cypress --save-dev` - Instalação do Cypress na pasta do projeto atual

## Execução do projeto

`npm run cy:open`- Executa o modo interativo do Cypress

`npm run cy:run` - Executa os testes no modo headless
